<?php
/**
 * Template para páginas.
 *
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<main id="main" role="main">

	<!-- Wrap para el contenido, en este caso no hay loop. -->
	<article id="post-<?php the_ID(); ?>">

		<!-- Cabecera. -->
		<div id="entrada__cabecera">
			<div id="entrada__titulo">
	      <h1>
					<?php echo get_the_title(); ?>
	      </h1>
			</div>
		</div>
		<!-- FIN de la cabecera. -->

		<!-- Wrap para eliminar el riesgo de overflow en Chrome. -->
		<div style="overflow: hidden">
			<!-- Contenido principal. -->
			<div id="entrada__contenido">

				<!-- Compartir en redes sociales. -->
				<div id="entrada__compartir">
					<a href="<?php echo 'https://twitter.com/intent/tweet?text=' . urlencode(get_the_title()) . '&url=' . urlencode(get_permalink()) . '&via=ANEM_mat'; ?>">
						<div class="icon is-large">
			  			<i class="fab fa-2x fa-twitter texto--twitter"></i>
			  		</div>
			  	</a>
		  		<a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_the_title() . ': ' . get_permalink()); ?>">
						<div class="icon is-large">
			  			<i class="fab fa-2x fa-facebook texto--facebook"></i>
			  		</div>
			  	</a>
		  		<a href="<?php echo 'https://www.telegram.me/share/url?url=' . urlencode(get_permalink()); ?>">
						<div class="icon is-large">
			  			<i class="fab fa-2x fa-telegram texto--telegram"></i>
			  		</div>
			  	</a>
					<a href="<?php echo 'https://wa.me/?text=' . urlencode(get_the_title() . ': ' . get_permalink()); ?>">
						<div class="icon is-large">
			  			<i class="fab fa-2x fa-whatsapp texto--whatsapp"></i>
			  		</div>
			  	</a>
		  		<a href="<?php echo 'mailto:?to=&subject=' . urlencode(get_the_title()) . '&body=' . urlencode(get_permalink()); ?>">
						<div class="icon is-large">
			  			<i class="far fa-2x fa-envelope texto--fucsia"></i>
			  		</div>
			  	</a>
		  		<a href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode(get_permalink()); ?>">
						<div class="icon is-large">
			  			<i class="fab fa-2x fa-linkedin texto--linkedin"></i>
			  		</div>
			  	</a>
				</div>
				<!-- FIN de los iconos para compartir en redes. -->

				<!-- Borde a la derecha del texto. -->
				<div id="entrada__borde-derecho">
				</div>
				<!-- FIN del borde a la derecha. -->

				<!-- Texto. -->
				<div id="entrada__texto">
					<?php
					if (have_posts()):
					  while (have_posts()) : the_post();
					    the_content();
					  endwhile;
					else:
					  echo '<p>Sorry, no posts matched your criteria.</p>';
					endif;
					?>
				</div>
				<!-- FIN del texto. -->

			</div>
			<!-- FIN del contenido principal. -->
		</div>
		<!-- FIN del wrap para evitar overflow. -->

	</article>
	<!-- FIN del wrap para el contenido. -->

</main>

<?php get_footer(); ?>
