<?php
/**
 * Template para el pie de página.
 *
 * @package anem-wp
 */
?>

</div><!-- #content -->

<footer id="colophon" class="site-footer section" role="contentinfo">
	<div class="container">
		<div class="columns">

			<div class="column is-narrow">
				<aside class="menu">
				  <p class="menu-label">
				    Contacta con nosotros
				  </p>
			  	<ul class="menu-list">
			  		<li><a href="<?php echo antispambot('mailto:contacto@anemat.com'); ?>"><?php echo antispambot('contacto@anemat.com'); ?></a></li>
			  	</ul>
				  <p class="menu-label">
				    En las redes sociales
				  </p>
			  	<ul class="menu-list">
			  		<li><a href="https://www.facebook.com/ANEM.mat/">
			  			<span class="icon texto--facebook">
						  	<i class="fab fa-facebook"></i>
							</span>
							Facebook
						</a></li>
			  		<li><a href="https://twitter.com/anem_mat">
			  			<span class="icon texto--twitter">
						  	<i class="fab fa-twitter"></i>
							</span>
							Twitter
						</a></li>
			  		<li><a href="https://www.instagram.com/anem.mat/">
			  			<span class="icon texto--instagram">
						  	<i class="fab fa-instagram"></i>
							</span>
							Instagram
						</a></li>
			  	</ul>
				</aside>
			</div>

			<div class="column is-pulled-left is-narrow">
				<aside class="menu">
				  <p class="menu-label">
				    Otras sociedades
				  </p>
			  	<?php echo wp_nav_menu( array(
							'theme_location'    => 'menu-sociedades',
							'depth'             => 0,
							'container'         => '',
							'menu_class'        => 'menu-list',
							)
						); 
					?>
				</aside>
			</div>

		</div>
	</div><!-- .container -->
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
