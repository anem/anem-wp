<?php
/**
 * Template para búsquedas.
 *
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<main id="main" class="site-main wrapper" role="main">

	<!-- Título de la página. -->
	<div id="archivo__cabecera">
		<div id="archivo__titulo">
			<form id="busqueda" role="search" method="get" class="busqueda" action="<?php echo home_url( '/' ); ?>" >
				<div class="busqueda__icono">
		  		<span class="icon">
		  			<i class="fas fa-search"></i>
		  		</span>
				</div>
				<div>
      		<input type="text" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="Buscar..." />
				</div>
			</form>
      <p>
      	Para buscar, escribe los términos arriba y presiona Enter.
      </p>
		</div>
	</div>
	<!-- FIN del título. -->

	<!-- Comprueba si hay entradas que mostrar. -->
	<?php if ( have_posts() ) : ?>

		<!-- Zona en la que aparecerán las entradas. -->
		<div class="container section">
			<div class="columns is-multiline">

				<!-- Inicio del loop. -->
				<?php while ( have_posts() ) : the_post(); ?>

					<!-- Incluye la tarjeta correspondiente. -->
					<?php get_template_part( 'template-parts/tarjeta' ); ?>
					<!-- FIN de la tarjeta. -->

				<?php endwhile; ?>
				<!-- FIN del loop. -->

			</div>
		</div>
		<!-- FIN de la zona de entradas. -->

		<!-- Páginas, si hacen falta. -->
		<div class="section">
			<div class="container">
				<?php 
					the_posts_pagination(
						array(
					    'prev_text' => __( '«', 'textdomain' ),
					    'next_text' => __( '»', 'textdomain' ),
						)
					); 
				?>
			</div>
		</div>
		<!-- FIN de las páginas. -->

	<?php else : ?>

		<!-- En caso de que no se hayan encontrado resultados. -->
		<div style="text-align: center; margin-top: 7rem">

  		<span class="icon is-large">
  			<i class="far fa-3x fa-frown"></i>
  		</span>

			<p style="margin-top: 2rem;"> No hemos encontrado nada que coincida con tu búsqueda. </p>

		</div>
		<!-- FIN del bloque sin resultados. -->

	<?php endif; ?>
	<!-- FIN de la comprobación de entradas. -->

</main>

<?php get_footer(); ?>
