<?php
/**
 * Template de la página principal (//).
 *
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main wrapper" role="main">

		<section id="banner-inicio" class="section is-paddingless">

			<div class="columns is-mobile is-vcentered">

				<div class="column is-2-mobile is-2-tablet is-4-desktop fondo--geometrico padding-s has-text-centered banner-inicio__izquierda">
			    <span style="display:inline-block;height:100%;vertical-align:middle;"></span>
					<img src="http://www.anem.es/archivos/media/logo-anem-2018.svg">
				</div>

				<div class="column is-10-mobile is-10-tablet is-8-desktop banner-inicio__derecha">
			      <h1 class="title">
			        <?php echo bloginfo( 'title' ); ?>
			      </h1>
			      <h2 class="subtitle is-italic is-family-secondary">
			        <?php echo bloginfo( 'description' ); ?>
			      </h2>
				</div>
			</div>

		</section>

		<section class="section is-medium">
			<div class="container">
				<div class="columns is-variable is-0-mobile is-8-tablet">
				  <div class="column">
				  	<div class="content">
				  		<span class="icon is-large texto--fucsia">
				  			<i class="fas fa-3x fa-bullhorn"></i>
				  		</span>
							<h3 class="title is-4">Representación</h3>
							<p class="is-family-secondary">
								Defendemos las reivindicaciones de los estudiantes a nivel tanto local como nacional. Para ello, convocamos dos veces al año una Asamblea General a la que acuden representantes de universidades de toda España.
							</p>
							<p class="is-family-secondary">
								Además, nos reunimos con otras asociaciones estudiantiles, mantenemos contacto con sociedades matemáticas y participamos en las reuniones que se convocan desde el Gobierno.
							</p>
						</div>
				  </div>
				  <div class="column">
				  	<div class="content">
				  		<span class="icon is-large texto--cian">
				  			<i class="fas fa-3x fa-users"></i>
				  		</span>
							<h3 class="title is-4">Actividades</h3>
							<p class="is-family-secondary">
								Junto con representantes y asociaciones de diferentes facultades, coordinamos la organización de actividades a nivel nacional, como por ejemplo la del día π.
							</p>
							<p class="is-family-secondary">
								Además, organizamos anualmente el <a class="enlace__fondo enlace__fondo--cian" href="enem.anem.es">Encuentro Nacional de Estudiantes de Matemáticas</a>, que sirve para unir a cientos de estudiantes de todos los puntos del país durante una semana.
							</p>
						</div>
				  </div>
				  <div class="column">
				  	<div class="content">
				  		<span class="icon is-large texto--verdecl">
				  			<i class="fas fa-3x fa-file-alt"></i>
				  		</span>
							<h3 class="title is-4">Publicaciones</h3>
							<p class="is-family-secondary">
								Publicamos tanto el <a class="enlace__fondo enlace__fondo--verdecl" href="http://www.anem.es/boletin-anem-rsme/">Boletín ANEM-RSME</a>, con información de actividades, becas o entrevistas, como la <a class="enlace__fondo enlace__fondo--verdecl" href="temat.es">revista divulgativa TEMat</a>, escrita y editada por alumnos.
							</p>
							<p class="is-family-secondary">
								Además, ponemos a disposición de todo el estudiantado recursos como los listados de <a class="enlace__fondo enlace__fondo--verdecl" href="anem.es/recursos/grados/">grados</a> y <a class="enlace__fondo enlace__fondo--verdecl" href="anem.es/recursos/masteres/">másteres</a> o la <a class="enlace__fondo enlace__fondo--verdecl" href="http://www.anem.es/guia/">guía para estudiantes</a> que acaban de empezar.
							</p>
						</div>
				  </div>
				</div>
				<a class="subtitle enlace-seccion is-pulled-right" href="//www.anem.es/quienes-somos/nuestra-historia/">Nuestra historia »</a>
			</div>
		</section>

		<section class="section is-large is-paddingless">

			<?php if ( get_theme_mod( 'anem_wp-opciones_adicional_check', '' ) ): ?>

				<div class="columns is-mobile">

					<div class="column is-2-mobile is-2-tablet is-4-desktop has-text-right padding-s" 
							 style="background-color: <?php echo get_theme_mod( 'anem_wp-opciones_adicional_color_fondo', '#003572' ); ?>">
			  		<span class="icon is-large" 
			  		      style="color: <?php echo get_theme_mod( 'anem_wp-opciones_adicional_color_icono', '#fff' ); ?>">
			  			<i class="<?php echo get_theme_mod( 'anem_wp-opciones_adicional_icono', '' ); ?> fa-3x"></i>
			  		</span>
					</div>

					<div class="column is-10-mobile is-10-tablet is-8-desktop" style="padding-bottom: 2em">
						<h3 class="title is-5"> 
							<?php echo get_theme_mod( 'anem_wp-opciones_adicional_titulo' ); ?> 
						</h3>
						<?php if ( get_theme_mod( 'anem_wp-opciones_adicional_subtitulo', '' ) ): ?>
							<p class="subtitle is-5"> 
								<?php echo get_theme_mod( 'anem_wp-opciones_adicional_subtitulo' ); ?> 
							</p>
						<?php else: ?>
						<?php endif ?>
						<p class="subtitle is-6"> <?php echo get_theme_mod( 'anem_wp-opciones_adicional_contenido' ); ?>  </p>
					</div>
				</div>

			<?php else: ?>

			<?php endif ?>

			<div class="columns is-mobile">

				<!-- Icono -->
				<div class="column is-2-mobile is-2-tablet is-4-desktop fondo--rosa is-vcentered has-text-right padding-s">
		  		<span class="icon is-large texto--verde">
		  			<i class="fas fa-3x fa-link"></i>
		  		</span>
				</div>
				<!-- -->

				<div class="column is-10-mobile is-10-tablet is-8-desktop padding-s">

			  	<a class="columns is-mobile is-vcentered padding-s mrfix" href="http://enem.anem.es">
			  		<div class="column is-narrow">
			    		<figure class="image frontpage-image">
			    			<img src="https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2Falbergueweb1.uva.es%2Fsmso2018%2Fimages%2Fescudo_uva.png&f=1&nofb=1" alt="Universidad de Valladolid">
			    		</figure>
			  		</div>
			  		<div class="column">
	    				<div class="content">
					      <h3 class="title is-5">
					        Encuentro Nacional de Estudiantes de Matemáticas
					      </h3>
					      <p class="subtitle is-6">
					        XXI ENEM Valladolid, julio de 2020
					      </p>
					    </div>
			  		</div>
			  	</a>
			  	
			  	<a class="columns is-mobile is-vcentered padding-s mrfix" href="https://www.anemat.com/guia/">
			  		<div class="column is-narrow">
			    		<figure class="image frontpage-image">
			    			<img src="http://www.anem.es/archivos/media/2019/06/logo-guia.png" alt="Guía para estudiantes de matemáticas">
			    		</figure>
			  		</div>
			  		<div class="column">
	    				<div class="content">
					      <h3 class="title is-5">
					        Guía para estudiantes de matemáticas
					      </h3>
					      <p class="subtitle is-6">
					        Recursos para quienes comienzan sus estudios universitarios
					      </p>
					    </div>
			  		</div>
			  	</a>
			  	
			  	<a class="columns is-mobile is-vcentered padding-s mrfix" href="https://temat.es">
			  		<div class="column is-narrow">
			    		<figure class="image frontpage-image">
			    			<img src="http://www.anem.es/archivos/media/2019/06/logo-temat.png" alt="Logo de la revista TEMat">
			    		</figure>
			  		</div>
			  		<div class="column">
	    				<div class="content">
					      <h3 class="title is-5">
					        Revista TEMat
					      </h3>
					      <p class="subtitle is-6">
					        Revista electrónica de divulgación de trabajos de estudiantes de matemáticas
					      </p>
					    </div>
			  		</div>
			  	</a>
			  	
			  	<div class="columns is-mobile is-vcentered padding-s mrfix">
			  		<div class="column is-narrow">
			    		<figure class="image frontpage-image">
			    			<img src="http://www.anem.es/archivos/media/2019/06/logo-listas-grados-masteres.png" alt="Listados de grados y másteres">
			    		</figure>
			  		</div>
			  		<div class="column">
	    				<div class="content">
					      <h3 class="title is-5">
					        Listados de grados y másteres
					      </h3>
					      <p class="subtitle is-6">
					        Consulta en qué universidades españolas puedes estudiar <a href="//www.anem.es/recursos/grados/"><b>grados</b></a> o <a href="//www.anem.es/recursos/masteres/"><b>másteres</b></a> de matemáticas
					      </p>
					    </div>
			  		</div>
			  	</div>

				</div>
			</div>
		</section>

		<section class="section is-medium">
			<h3 class="title is-3 has-text-centered"> Últimas noticias <a class="subtitle enlace-seccion" href="//www.anem.es/blog/">ver&nbsp;más&nbsp;en&nbsp;el&nbsp;blog&nbsp;»</a> </h3>
			
			<div class="container section">
				<div class="columns is-multiline">
					<?php
						$args = array(
					    'post_type' => 'post',
					    'posts_per_page' => '3',
							'ignore_sticky_posts' => 1,
						);

						$query = new WP_Query( $args );

						while ( $query -> have_posts()) : $query -> the_post();

							get_template_part( 'template-parts/tarjeta' );

						endwhile;

						wp_reset_query();
					?>
				</div>
			</div>
		</section>

		<section class="section is-medium has-background-white">
			<h3 class="title is-3 has-text-centered"> Socios de la ANEM <a class="subtitle enlace-seccion" href="//www.anem.es/socios/">¡apúntate!&nbsp;»</a> </h3>

			<div class="columns is-multiline is-mobile">

				<?php 
					$menu_name = 'menu-socios';
					$locations = get_nav_menu_locations();
					$menu      = wp_get_nav_menu_object( $locations[ $menu_name ] );
					$socios    = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );

					foreach( $socios as $socio ):
						$socio_enlace = $socio->url;
						$socio_nombre = $socio->title;
						echo '
							<div class="column is-2-desktop is-3-tablet is-one-third-mobile">
								<div class="card">
									<div class="card-image">
										<figure class="image is-2by1">
											<img src="' . $socio_enlace . '" alt="' . $socio_nombre . '">
										</figure>
									</div>
								</div>
							</div>
						';
					endforeach;
				?>

			</div>
		</section>

	</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>