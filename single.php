<?php
/**
 * Template para entradas.
 *
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<main id="main" role="main">

	<!-- Empieza el loop. -->
	<?php while ( have_posts() ) : the_post(); ?>

		<!-- Wrap para el contenido. -->
		<article id="post-<?php the_ID(); ?>">

			<!-- Cabecera. -->
			<div id="entrada__cabecera">
				<div id="entrada__titulo">
		  		<p class="entrada__titulo--subtitulo"> <?php echo get_the_category_list( esc_html__( ', ', 'bulmapress' ) ); ?> </p>
		      <h1>
						<?php echo get_the_title(); ?>
		      </h1>
		  		<p class="entrada__titulo--subtitulo"> <?php echo '<time datetime="' . get_the_date( 'c' ) . '">' . get_the_date( '' ) . '</time>'; ?></p>
				</div>
			</div>
			<!-- FIN de la cabecera. -->

			<!-- Wrap para eliminar el riesgo de overflow en Chrome. -->
			<div style="overflow: hidden">
				<!-- Contenido principal. -->
				<div id="entrada__contenido">

					<!-- Compartir en redes sociales. -->
					<div id="entrada__compartir">
						<a href="<?php echo 'https://twitter.com/intent/tweet?text=' . urlencode(get_the_title()) . '&url=' . urlencode(get_permalink()) . '&via=ANEM_mat'; ?>">
							<div class="icon is-large">
				  			<i class="fab fa-2x fa-twitter texto--twitter"></i>
				  		</div>
				  	</a>
			  		<a href="<?php echo 'https://www.facebook.com/sharer/sharer.php?u=' . urlencode(get_the_title() . ': ' . get_permalink()); ?>">
							<div class="icon is-large">
				  			<i class="fab fa-2x fa-facebook texto--facebook"></i>
				  		</div>
				  	</a>
			  		<a href="<?php echo 'https://www.telegram.me/share/url?url=' . urlencode(get_permalink()); ?>">
							<div class="icon is-large">
				  			<i class="fab fa-2x fa-telegram texto--telegram"></i>
				  		</div>
				  	</a>
						<a href="<?php echo 'https://wa.me/?text=' . urlencode(get_the_title() . ': ' . get_permalink()); ?>">
							<div class="icon is-large">
				  			<i class="fab fa-2x fa-whatsapp texto--whatsapp"></i>
				  		</div>
				  	</a>
			  		<a href="<?php echo 'mailto:?to=&subject=' . urlencode(get_the_title()) . '&body=' . urlencode(get_permalink()); ?>">
							<div class="icon is-large">
				  			<i class="far fa-2x fa-envelope texto--fucsia"></i>
				  		</div>
				  	</a>
			  		<a href="<?php echo 'https://www.linkedin.com/shareArticle?mini=true&url=' . urlencode(get_permalink()); ?>">
							<div class="icon is-large">
				  			<i class="fab fa-2x fa-linkedin texto--linkedin"></i>
				  		</div>
				  	</a>
					</div>
					<!-- FIN de los iconos para compartir en redes. -->

					<!-- Borde a la derecha del texto. -->
					<div id="entrada__borde-derecho">
					</div>
					<!-- FIN del borde a la derecha. -->

					<!-- Texto. -->
					<div id="entrada__texto">
						<?php the_content(); ?>
					</div>
					<!-- FIN del texto. -->

				</div>
				<!-- FIN del contenido principal. -->
			</div>
			<!-- FIN del wrap para evitar overflow. -->

		</article>
		<!-- FIN del wrap para el contenido. -->

		<!-- Entradas anterior/siguente. -->
		<section class="section">
			<div class="level is-tablet">
			
				<?php 
					$next_post = get_next_post();
        	$prev_post = get_previous_post();

        	/* Entrada anterior. */
        	if($prev_post) :
        		echo '
        		<a class="level-left" href="' . get_permalink( $prev_post->ID ) . '">
        			<div class="level-item">
							  <img src="' . get_the_post_thumbnail_url( $prev_post->ID ) . '" width="128px" height="64px">
        			</div>
        			<div class="level-item">
        				<h5 class="title is-5">' . get_the_title( $prev_post->ID ) . '</h5>
        			</div>
      			</a>
        		';
        	else :
        		echo '<div class="level-left"></div>';
        	endif;
        	/* FIN de la entrada anterior.

        	/* Separador para versión `mobile`. */
        	echo '<div class="show-only-mobile" style="height:2em;"></div>';
        	/* FIN del separador. */

        	/* Entrada siguiente. */
        	if($next_post) :
        		echo '
        		<a class="level-right" href="' . get_permalink( $next_post->ID ) . '">
        			<div class="level-item">
        				<h5 class="title is-5">' . get_the_title( $next_post->ID ) . '</h5>
        			</div>
        			<div class="level-item">
							  <img src="' . get_the_post_thumbnail_url( $next_post->ID ) . '" width="128px" height="64px">
        			</div>
      			</a>
        		';
        	else :
        		echo '<div class="level-right"></div>';
        	endif;
        	/* FIN de la entrada siguiente. */
				?>

			</div>
		</section>
		<!-- FIN de las entradas anterior/siguente. -->

	<?php endwhile; ?>
	<!-- FIN del loop. -->

</main>

<?php get_footer(); ?>
