<?php
/**
 * Template para el encabezado y la navegación.
 *
 * @link https://developer.wordpress.org/reference/functions/get_template_part/
 * @package anem-wp
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="is-fullheight">
<head>
	<!-- Codificación del documento (en general, utf8) -->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	
	<!-- Información del viewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="site">
		<header id="header">
			<nav class="navbar is-fixed-top" role="navigation">
				<div id="site-navigation" class="container">
					<div class="navbar-brand">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="navbar-item" rel="home">
							<img src="http://www.anem.es/archivos/media/logo-anem-2018.svg" alt="<?php echo get_bloginfo( 'title' );?>" width="48" height="48">
						</a>
						<button id="menu-toggle" class="navbar-burger" style="border: none; background: inherit; color: inherit; height: auto;" aria-controls="menu-principal" aria-expanded="false" data-target="menu-principal">
							<span></span>
							<span></span>
							<span></span>
						</button>
					</div>
					<div id="menu-principal" class="navbar-menu">
						<div class="navbar-start">
							<?php navegacion(); ?>
						</div>
						<div class="navbar-end">
							<div id="busqueda-nav" class="navbar-item has-dropdown is-hoverable">
								<a class="navbar-link" href="<?php echo home_url( '/' ) . '?s='; ?>">
									<span class="icon">
						  			<i class="fas fa-search"></i>
						  		</span>
						  		<span class="is-hidden-tablet">&nbsp;Buscar</span>
						  	</a>
								<div class="navbar-dropdown is-right is-hidden-mobile">
									<div class="navbar-item" style="width:20em;">
										<form role="search" method="get" id="busqueda-nav__form" action="<?php echo home_url( '/' ); ?>" >
											<input id="s" name="s" class="busqueda-nav__input" type="text" placeholder="Buscar...">
											<input id="searchsubmit" type="submit" value="Buscar" style="display: none">
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		</header>

		<div id="content" class="site-content">

