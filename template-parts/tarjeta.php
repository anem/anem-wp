<?php
/**
 * Template para tarjetas de entradas y páginas.
 *
 * @link https://developer.wordpress.org/reference/functions/get_template_part/
 * @package anem-wp
 */
?>

<!-- Tarjeta para cada entrada. -->
<article id="post-<?php the_ID(); ?>" class="tarjeta">
	<a href="<?php echo get_the_permalink(); ?>">
		<div class="tarjeta__contenido">
    	<!-- Imagen destacada. -->
	    <figure>
	    	<!-- Si no hay imagen destacada, incluye una por defecto. -->
	    	<?php if ( has_post_thumbnail() ) : ?>
	      	<img src="<?php echo get_the_post_thumbnail_url();?>" alt="<?php echo the_title(); ?>">
	      <?php else: ?>
	      	<img src="<?php echo get_template_directory_uri() . '/frontend/img/fondos/thmb_fondos_aleatorios_' . rand(1, 29) . '.jpg'; ?>" alt="<?php echo the_title(); ?>">
	      <?php endif; ?>
	      <!-- Tapa la imagen en caso de hover. -->
	    	<div class="tarjeta__overlay">
	    		<?php
	    			$categories = get_the_category();
	    			// Imprime la primera categoría.
	    			if ( ! empty( $categories ) ) {
						    echo '
						    	<p>
    					  		<span class="icon">
							  			<i class="fas fa-tag"></i>
							  		</span>
							  	' . esc_html( $categories[0]->name ) . '
						  		</p>';   
						}
					?>
					<!-- Imprime la fecha y la hora. -->
		      <p>
			  		<span class="icon">
			  			<i class="far fa-calendar-alt"></i>
			  		</span>
		      	<time datetime="<?php echo get_the_date( 'c' ); ?>"><?php echo get_the_date( '' ); ?></time>
	      	</p>
	    	</div>
	    </figure>
	    <!-- FIN de la imagen destacada. -->

	    <!-- Título y descripción. -->
	    <div class="tarjeta__texto">
	    	<h2><?php echo the_title(); ?></h2>
	      <?php echo the_excerpt(); ?>
    	</div>
    	<!-- FIN del título y descripción. -->
	  </div>
	</a>
</article>
<!-- FIN de la tarjeta. -->