<?php
/**
 * Sección de comentarios. Por el momento deshabilitada, pero
 * el archivo permanece para evitar que se sobreescriba
 * con la página por defecto.
 *
 * @package anem-wp
 */
?>

<?php
/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ):
	return;
endif;
?>