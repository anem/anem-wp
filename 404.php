<?php
/**
 * Template para la página 404.
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<main id="main" class="site-main wrapper" role="main">

	<!-- Título de la página. -->
	<div id="archivo__cabecera">
		<div id="archivo__titulo">
			<form id="busqueda" role="search" method="get" class="busqueda" action="<?php echo home_url( '/' ); ?>" >
				<div class="busqueda__icono">
		  		<span class="icon">
		  			<i class="fas fa-search"></i>
		  		</span>
				</div>
				<div>
      		<input type="text" name="s" id="s" placeholder="Buscar..." />
				</div>
			</form>
		</div>
	</div>
	<!-- FIN del título. -->

	<!-- Explicación sobre el error 404. -->
	<div style="text-align: center; margin-top: 7rem; padding: 1em;">

		<span class="icon is-large">
			<i class="far fa-3x fa-question-circle"></i>
		</span>

		<p style="margin-top: 2rem;"> ¡Vaya! Parece que la página que buscas no existe. Prueba a usar el buscador. Si sigues sin encontrar el contenido, puedes ponerte en contacto con nosotros en <a href="mailto:contacto@anemat.com">contacto@anemat.com</a>. </p>

	</div>
	<!-- FIN del bloque de explicación. -->

</main>

<?php get_footer(); ?>
