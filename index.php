<?php
/**
 * Archivo principal, se limita a comprobar el loop y mostrar
 * las entradas que hay. Solo en uso en la subsección del blog.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * @package anem-wp
 */
?>

<?php get_header(); ?>

<main id="main" class="site-main wrapper" role="main">

	<!-- Comprueba si hay entradas que mostrar. -->
	<?php if ( have_posts() ) : ?>

		<!-- Título de la página. -->
		<div id="archivo__cabecera">
			<div id="archivo__titulo">
	      <h1>
					Blog
	      </h1>
	      <p>
	      	Todas las entradas del blog de la ANEM.
	      </p>
			</div>
		</div>
		<!-- FIN del título. -->

		<!-- Zona en la que aparecerán las entradas. -->
		<div class="container section">
			<div class="columns is-multiline">

				<!-- Inicio del loop. -->
				<?php while ( have_posts() ) : the_post(); ?>

					<!-- Incluye la tarjeta correspondiente. -->
					<?php get_template_part( 'template-parts/tarjeta' ); ?>
					<!-- FIN de la tarjeta. -->

				<?php endwhile; ?>
				<!-- FIN del loop. -->

			</div>
		</div>
		<!-- FIN de la zona de entradas. -->

		<!-- Páginas, si hacen falta. -->
		<div class="section">
			<div class="container">
				<?php 
					the_posts_pagination(
						array(
					    'prev_text' => __( '«', 'textdomain' ),
					    'next_text' => __( '»', 'textdomain' ),
						)
					); 
				?>
			</div>
		</div>
		<!-- FIN de las páginas. -->

	<?php else : ?>
	<?php endif; ?>
	<!-- FIN de la comprobación de entradas. -->

</main>

<?php get_footer(); ?>
