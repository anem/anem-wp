<?php
/**
 * Funciones para cargar.
 *
 * @package anem-wp
 */


// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
 

/**
 * Class Name: Navwalker
 * Plugin Name: Bulma Navwalker
 * Plugin URI:  https://github.com/Poruno/Bulma-Navwalker
 * Description: An extended Wordpress Navwalker object that displays Bulma framework's Navbar https://bulma.io/ in Wordpress.
 * Author: Carlo Operio - https://www.linkedin.com/in/carlooperio/, Bulma-Framework
 * Author URI: https://github.com/wp-bootstrap
 * License: GPL-3.0+
 * License URI: https://github.com/Poruno/Bulma-Navwalker/blob/master/LICENSE
 */

    class bulmapress_navwalker extends Walker_Nav_Menu {

        public function start_lvl( &$output, $depth = 0, $args = array() ) {
            
            $output .= "<div class='navbar-dropdown'>";
        }

        public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

            $liClasses = 'navbar-item ';

            $hasChildren = $args->walker->has_children;
            $liClasses .= $hasChildren? " has-dropdown is-hoverable": "";

            if($hasChildren){
                $output .= "<div class='".$liClasses."'>";
                $output .= "\n<a class='navbar-link' href='".$item->url."'>".$item->title."</a>";
            }
            else {
                $output .= "<a class='".$liClasses."' href='".$item->url."'>".$item->title."</a>";
            }

            // Adds has_children class to the item so end_el can determine if the current element has children
            if ( $hasChildren ) {
                $item->classes[] = 'has_children';
            }
        }
        
        public function end_el(&$output, $item, $depth = 0, $args = array(), $id = 0 ){

            if(in_array("has_children", $item->classes)) {

                $output .= "</div>";
            }
        }

        public function end_lvl (&$output, $depth = 0, $args = array()) {

            $output .= "</div>";
        }
    }

/* END of Bulma Navwalker. */

/**
 * Class Name: Bulmapress
 * Class URI: http://bulmapress.com/
 * Description: A modern wordpress starter theme built on top of underscores (by Autommatic) with Bulma.io mobile-first flexbox frontend.
 * Author: Scops
 * Author URI: http://scops.com
Version: 0.0.1
 * License: GNU General Public License v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.html

 * Bulmapress is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
 * Underscores is distributed under the terms of the GNU GPL v2 or later.
 */


    if ( ! function_exists( 'bulmapress_setup' ) ) {

        /**
         * Sets up theme defaults and registers support for various WordPress features.
         *
         * Note that this function is hooked into the after_setup_theme hook, which
         * runs before the init hook. The init hook is too late for some features, such
         * as indicating support for post thumbnails.
         */
        
        function bulmapress_setup() {

            /* Add default posts and comments RSS feed links to head. */
            add_theme_support( 'automatic-feed-links' );

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support( 'title-tag' );

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support( 'html5', array(
                'search-form',
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
                'widgets',
                ) );

            /* Permite utilizar imágenes para cada entrada del blog. */

            add_theme_support( 'post-thumbnails' );

            /* Menús disponibles para el tema. */

            if ( ! function_exists( 'crear_menus' ) ) {
                function crear_menus () {
                    
                    add_theme_support( 'menus' ); 
                    
                    register_nav_menus( array(
                        'menu-navigation' => esc_html( 'Navegación' ),
                        'menu-sociedades' => esc_html( 'Otras sociedades' ),
                        'menu-socios'     => esc_html( 'Socios de la ANEM' ),
                        ) );
                }
            }
            
            add_action( 'init', 'crear_menus' );

            /* Navegación para el tema, tomada del original `bulmapress_navigation`. */

            if ( ! function_exists( 'navegacion' ) ) {
                function navegacion()
                {
                    wp_nav_menu( array(
                        'theme_location'    => 'menu-navigation',
                        'depth'             => 0,
                        'container'         => 'div id="navigation"',
                        'menu_class'        => 'navbar-end',
                        'fallback_cb'       => 'bulmapress_navwalker::fallback',
                        'walker'            => new bulmapress_navwalker()
                        )
                    );
                }
            }
        }
    }

    add_action( 'after_setup_theme', 'bulmapress_setup' );

    if ( ! function_exists( 'edicion_personalizada' ) ) {
        /**
         * Add postMessage support for site title and description for the Theme Customizer.
         *
         * @param WP_Customize_Manager $wp_customize Theme Customizer object.
         */
        function edicion_personalizada( $wp_customize ) {
            $wp_customize->remove_section("colors");
            $wp_customize->remove_section("background_image");

            $wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
            $wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
        }
    }

    add_action( 'customize_register', 'edicion_personalizada' );

/* END of Bulmapress. */

/**
 * Plugin Name: T5 Page to Seite
 * Description: Ersetzt <code>/page/</code> durch <code>/seite/</code>.
 * Author:      Thomas Scholz <info@toscho.de>
 * Author URI:  http://toscho.de
 * License:     MIT
 * License URI: http://www.opensource.org/licenses/mit-license.php
 */

    if ( ! function_exists( 't5_page_to_seite' ) )
    {
        register_activation_hook(   __FILE__ , 't5_flush_rewrite_on_init' );
        register_deactivation_hook( __FILE__ , 't5_flush_rewrite_on_init' );
        add_action( 'init', 't5_page_to_seite' );

        function t5_page_to_seite()
        {
            $GLOBALS['wp_rewrite']->author_base        = 'autor';
            $GLOBALS['wp_rewrite']->search_base        = 'buscar';
            $GLOBALS['wp_rewrite']->comments_base      = 'comentarios';
            $GLOBALS['wp_rewrite']->pagination_base    = 'pagina';
        }

        function t5_flush_rewrite_on_init()
        {
            add_action( 'init', 'flush_rewrite_rules', 11 );
        }
    }

/* END of T5 Page to Seite. */

/** 
 * Personaliza la importación de favicons con más opciones.
 *
 * Cortesía de faviconit.com. 
 */

if ( ! function_exists( 'importar_favicons' ) ) {
    function importar_favicons() {
        $directorio = '/frontend/img/favicons';
        $nombre = 'logo-anem';
        echo '
        <link rel="shortcut icon" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '.ico">
        <link rel="icon" sizes="16x16 32x32 64x64" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '.ico">
        <link rel="icon" type="image/png" sizes="196x196" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-192.png">
        <link rel="icon" type="image/png" sizes="160x160" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-160.png">
        <link rel="icon" type="image/png" sizes="96x96" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-96.png">
        <link rel="icon" type="image/png" sizes="64x64" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-64.png">
        <link rel="icon" type="image/png" sizes="32x32" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-16.png">
        <link rel="apple-touch-icon" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-57.png">
        <link rel="apple-touch-icon" sizes="114x114" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-114.png">
        <link rel="apple-touch-icon" sizes="72x72" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-72.png">
        <link rel="apple-touch-icon" sizes="144x144" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-144.png">
        <link rel="apple-touch-icon" sizes="60x60" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-60.png">
        <link rel="apple-touch-icon" sizes="120x120" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-120.png">
        <link rel="apple-touch-icon" sizes="76x76" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-76.png">
        <link rel="apple-touch-icon" sizes="152x152" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-180.png">
        <meta name="msapplication-TileColor" content="#FFFFFF">
        <meta name="msapplication-TileImage" content="' . get_template_directory_uri() . $directorio . '/' . $nombre . '-144.png">
        <meta name="msapplication-config" content="' . get_template_directory_uri() . $directorio . '/browserconfig.xml">';
    }
}

/**
 * Carga el script de Google Analytics.
 */

if ( ! function_exists( 'cargar_GA' ) ) {
    function cargar_GA() {
        $codigo = 'UA-123216732-2';
        echo "
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', '" . $codigo . "', 'auto');
            ga('send', 'pageview');
        </script>";
    }
}

/** 
 * Añade GA y los faviconos al head.
 */

add_action( 'wp_head', 'importar_favicons' );
add_action( 'wp_head', 'cargar_GA' );

/**
 * Elimina tags sobrantes.
 */

if ( ! function_exists( 'limpiar_wp_head' ) ) {
    function limpiar_wp_head() {
        remove_action( 'wp_head', 'wp_site_icon', 99 );
        remove_action( 'login_head', 'wp_site_icon', 99 );
        remove_action( 'admin_head', 'wp_site_icon', 99 );
        remove_action( 'wp_head', 'wp_generator' );
    }
}

add_action( 'wp_head', 'limpiar_wp_head' );

/**
 * Permite cargar scripts de manera asíncrona.
 */

if ( ! function_exists( 'add_async_attribute' ) ) {
    function add_async_attribute($tag, $handle) {
        if ( 'fontawesome' !== $handle )
            return $tag;
        return str_replace( ' src', ' async="async" src', $tag );
    }
}

add_filter( 'script_loader_tag', 'add_async_attribute', 10, 2 );

/**
 * Carga los archivos JavaScript y CSS necesarios.
 */

if ( ! function_exists( 'cargar_js_css' ) ) {
    function cargar_js_css() {
        /** CSS general. */
        wp_enqueue_style( 'estilos', get_template_directory_uri() . '/frontend/css/estilos.min.css' );

        /** Tipografías de Google Fonts, iconos de FontAwesome. */
        wp_enqueue_style( 'merriweather', '//fonts.googleapis.com/css?family=Noto+Serif:400,400i,700&display=fallback' );
        wp_enqueue_style( 'fontawesome', "https://use.fontawesome.com/releases/v5.2.0/css/all.css" );

        /** Carga los archivos de JavaScript. */
        wp_enqueue_script( 'scripts', get_template_directory_uri() . '/frontend/js/funciones.min.js', array(), '20190716', false );
        wp_enqueue_script( 'navegacion', get_template_directory_uri() . '/frontend/js/navigation.js', array(), '20200611', true );
    }
}

add_action( 'wp_enqueue_scripts', 'cargar_js_css' );

/**
 * Configuración del panel adicional.
 */

function my_customizer_sanitize_checkbox( $input ) {
    // https://danielmilner.com/the-wordpress-customizer-and-checkboxes/
    if ( $input === true || $input === '1' ) {
        return '1';
    }
    return '';
}

function anemwp_customize_panel_adicional( $wp_customize ) {

    // Creación del panel.

    $wp_customize->add_panel( 'anem-wp_panel', array(
        'priority'       => 10,
        'title'          => 'Personalización del tema',
        'description'    => 'Opciones para personalizar diferentes secciones del tema.',
    ));

    // Creación de la sección.

    $wp_customize->add_section( 'anem-wp_adicional', array(
        'title'          => 'Panel adicional',
        'description'    => 'Controla la apariencia del panel adicional que aparece por encima de los enlaces de la portada.',
        'panel'          => 'anem-wp_panel',
    ));

    // Activación.

    $wp_customize->add_setting('anem_wp-opciones_adicional_check', array('type' => 'theme_mod', 'default' => ''));

    $wp_customize->add_control('anem-wp_adicional_check', array(
        'settings' => 'anem_wp-opciones_adicional_check',
        'label'    => 'Mostrar el panel adicional.',
        'section'  => 'anem-wp_adicional',
        'type'     => 'checkbox',
    ));

    // Título.

    $wp_customize->add_setting('anem_wp-opciones_adicional_titulo', array('type' => 'theme_mod'));

    $wp_customize->add_control('anem-wp_adicional_titulo', array(
        'label'          => 'Título',
        'description'    => 'Título de la sección a añadir.',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_titulo',
    ));

    // Subtítulo.

    $wp_customize->add_setting('anem_wp-opciones_adicional_subtitulo', array('type' => 'theme_mod'));

    $wp_customize->add_control('anem-wp_adicional_subtitulo', array(
        'label'          => 'Subtítulo',
        'description'    => 'Si se deja el campo vacío no se mostrará.',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_subtitulo',
    ));

    // Contenido.

    $wp_customize->add_setting('anem_wp-opciones_adicional_contenido', array('type' => 'theme_mod'));

    $wp_customize->add_control('anem-wp_adicional_contenido', array(
        'label'          => 'Contenido',
        'description'    => 'Código que se añadirá a la sección. Pueden añadirse etiquetas HTML. Es recomendable envolver los párrafos en etiquetas `p`, con la clase `subtitle is-6`.',
        'type'           => 'textarea',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_contenido',
    ));

    // Color de fondo.

    $wp_customize->add_setting('anem_wp-opciones_adicional_color_fondo', 
        array('type'     => 'theme_mod', 'sanitize_callback' => 'sanitize_hex_color'));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'anem-wp_adicional_color_fondo', array(
        'label'          => 'Color de fondo',
        'description'    => 'Color de fondo del panel que incluye el icono.',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_color_fondo',
    )));

    // Color del icono.

    $wp_customize->add_setting('anem_wp-opciones_adicional_color_icono', 
        array('type'     => 'theme_mod', 'sanitize_callback' => 'sanitize_hex_color'));

    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'anem-wp_adicional_color_icono', array(
        'label'          => 'Color del icono',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_color_icono',
    ))); 

    // Código del icono.

    $wp_customize->add_setting('anem_wp-opciones_adicional_icono', array('type' => 'theme_mod'));

    $wp_customize->add_control('anem-wp_adicional_icono', array(
        'label'          => 'Icono',
        'description'    => 'Código de FontAwesome del icono. Hay que incluir los dos descriptores (prefijo e icono), por ejemplo `fas fa-info-circle`.',
        'section'        => 'anem-wp_adicional',
        'settings'       => 'anem_wp-opciones_adicional_icono',
    ));

}

add_action( 'customize_register', 'anemwp_customize_panel_adicional' );





